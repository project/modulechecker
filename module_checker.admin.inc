<?php

/**
 * @file
 * Administrative page callbacks for module checker module.
 */

/**
 * This code creates the table of all module on the site.
 *
 * Page for all module.
 */
function module_checker_total_module_list() {
  // Configure the table header columns.
  $header = array(
    t('Module Name'),
    t('Machine Name'),
    t('Module Type'),
    t('Enable/Disable'),
  );
  // Fetch all the result.
  $results = system_rebuild_module_data();
  // Configure the table rows.
  $rows = array();
  // Check if variable $results is clean for foreach.
  if (is_array($results) || is_object($results)) {
    // Loop through $results.
    foreach ($results as $arrayname => $content) {
      $rows[] = array(
        check_plain($content->info['name']),
        $arrayname,
        $content->info['package'],
        $content->status,
      );
    }
  }

  // Render table.
  $render_array['maintable'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  return $render_array;
}

/**
 * This code is used to the detail module count.
 *
 * This function used to split all the module in a different group
 * (Ex. Core, contributed or custom, etc.).
 */
function module_checker_detail_list() {
  $results = system_rebuild_module_data();

  $core = array();
  $test = array();
  $contrib = array();
  $custom = array();
  $other = array();
  if (is_array($results) || is_object($results)) {
    foreach ($results as $machinename => $content) {
      if ($content->info['package'] == 'Testing' && $content->info['project'] == 'drupal') {
        $test[] = array(
          'name' => check_plain($content->info['name']),
          'machinename' => $machinename,
          'type' => $content->info['package'],
          'status' => $content->status,
        );
      }
      elseif ($content->info['package'] == 'Core' && $content->info['project'] == 'drupal') {
        $core[] = array(
          'name' => check_plain($content->info['name']),
          'machinename' => $machinename,
          'type' => $content->info['package'],
          'status' => $content->status,
        );
      }
      elseif ($content->info['package'] == 'Other') {
        $other[] = array(
          'name' => check_plain($content->info['name']),
          'machinename' => $machinename,
          'type' => $content->info['package'],
          'status' => $content->status,
        );
      }
      elseif (isset($content->info['datestamp'])) {
        if ($content->info['package'] != 'Other') {
          $contrib[] = array(
            'name' => check_plain($content->info['name']),
            'machinename' => $machinename,
            'type' => $content->info['package'],
            'status' => $content->status,
          );
        }
      }
      else {
        $site_url = "https://www.drupal.org/project/" . $machinename;
        $error_report = module_checker_is_404($site_url);
        if ($error_report == 1) {
          $contrib[] = array(
            'name' => check_plain($content->info['name']),
            'machinename' => $machinename,
            'type' => $content->info['package'],
            'status' => $content->status,
          );
        }
        else {
          $custom[] = array(
            'name' => check_plain($content->info['name']),
            'machinename' => $machinename,
            'type' => $content->info['package'],
            'status' => $content->status,
          );
        }
      }
    }
  }
  // Check the enable and disable count for test module.
  $test_enable = 0;
  $test_disable = 0;
  foreach ($test as $value) {
    if ($value['status'] == 1) {
      $test_enable = $test_enable + 1;
    }
    else {
      $test_disable = $test_disable + 1;
    }
  }
  // Check the enable and disable count for core module.
  $core_enable = 0;
  $core_disable = 0;
  foreach ($core as $value) {
    if ($value['status'] == 1) {
      $core_enable = $core_enable + 1;
    }
    else {
      $core_disable = $core_disable + 1;
    }
  }
  // Check the enable and disable count for other module.
  $other_enable = 0;
  $other_disable = 0;
  foreach ($other as $value) {
    if ($value['status'] == 1) {
      $other_enable = $other_enable + 1;
    }
    else {
      $other_disable = $other_disable + 1;
    }
  }
  // Check the enable and disable count for contributed module.
  $contrib_enable = 0;
  $contrib_disable = 0;
  foreach ($contrib as $value) {
    if ($value['status'] == 1) {
      $contrib_enable = $contrib_enable + 1;
    }
    else {
      $contrib_disable = $contrib_disable + 1;
    }
  }
  // Check the enable and disable count for custom module.
  $custom_enable = 0;
  $custom_disable = 0;
  foreach ($custom as $value) {
    if ($value['status'] == 1) {
      $custom_enable = $custom_enable + 1;
    }
    else {
      $custom_disable = $custom_disable + 1;
    }
  }
  $header = array(
    t('Module Status'),
    t('Module Count'),
    t('Module Enable'),
    t('Module Disable'),
  );

  $rows = array(
    "Test" => array(
      "Testing Module",
      count($test),
      $test_enable,
      $test_disable,
    ),
    "Core" => array(
      "Core Module",
      count($core),
      $core_enable,
      $core_disable,
    ),
    "contrib" => array(
      "Contribute Module",
      count($contrib),
      $contrib_enable,
      $contrib_disable,
    ),
    "custom" => array(
      "Custom Module",
      count($custom),
      $custom_enable,
      $custom_disable,
    ),
    "other" => array(
      "Other Module",
      count($other),
      $other_enable,
      $other_disable,
    ),
  );
  // Render table.
  $render_array['maintable'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  return $render_array;
}

/**
 * This is the helper function for check the modulee in drupal.org.
 *
 * @param string $url
 *   Sent the module URL to check that module is exist or not.
 *
 * @return numeric
 *   Return the cureent module is exist or not in the drupal.org site
 */
function module_checker_is_404($url) {
  $handle = curl_init($url);
  curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);

  /* Get the HTML or whatever is linked in $url.
  $response = curl_exec($handle);*/

  /* Check for 404 (file not found). */
  $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
  curl_close($handle);

  /* If the document has loaded successfully without any redirection or error */
  if ($httpCode >= 200 && $httpCode < 300) {
    return 1;
  }
  else {
    return 0;
  }
}

/**
 * This function used to print the custom module in the table format.
 */
function module_checker_custom_module() {
  $results = system_rebuild_module_data();
  $custom = array();
  if (is_array($results) || is_object($results)) {
    foreach ($results as $machinename => $content) {
      if (!isset($content->info['datestamp'])) {
        $site_url = "https://www.drupal.org/project/" . $machinename;
        $error_report = module_checker_is_404($site_url);
        if ($error_report == 0) {
          $custom[] = array(
            'name' => check_plain($content->info['name']),
            'machinename' => $machinename,
            'type' => $content->info['package'],
            'status' => $content->status,
          );
        }
      }
    }
  }
  $header = array(
    t('Module Name'),
    t('Machine Name'),
    t('Module Type'),
    t('Enable/Disable'),
  );
  // Render table.
  $render_array['maintable'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $custom,
  );
  return $render_array;
}

/**
 * This function used to print the contribute module in the table format.
 */
function module_checker_contribute_module() {
  $results = system_rebuild_module_data();
  $contrib = array();
  if (is_array($results) || is_object($results)) {
    foreach ($results as $machinename => $content) {
      if (!($content->info['package'] == 'Core' || $content->info['package'] == 'Testing' || $content->info['package'] == 'Other')) {
        if (isset($content->info['datestamp'])) {
          $contrib[] = array(
            'name' => check_plain($content->info['name']),
            'machinename' => $machinename,
            'type' => $content->info['package'],
            'status' => $content->status,
          );
        }
      }
    }
  }
  $header = array(
    t('Module Name'),
    t('Machine Name'),
    t('Module Type'),
    t('Enable/Disable'),
  );
  // Render table.
  $render_array['maintable'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $contrib,
  );
  return $render_array;
}

/**
 * This function used to print the test module in the table format.
 */
function module_checker_test_module() {
  $results = system_rebuild_module_data();
  $test = array();
  if (is_array($results) || is_object($results)) {
    foreach ($results as $machinename => $content) {
      if ($content->info['package'] == 'Testing') {
        $test[] = array(
          'name' => check_plain($content->info['name']),
          'machinename' => $machinename,
          'type' => $content->info['package'],
          'status' => $content->status,
        );
      }
    }
  }
  $header = array(
    t('Module Name'),
    t('Machine Name'),
    t('Module Type'),
    t('Enable/Disable'),
  );
  // Render table.
  $render_array['maintable'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $test,
  );
  return $render_array;
}

/**
 * This function used to print the core module in the table format.
 */
function module_checker_core_module() {
  $results = system_rebuild_module_data();
  $core = array();
  if (is_array($results) || is_object($results)) {
    foreach ($results as $machinename => $content) {
      if ($content->info['package'] == 'Core' && $content->info['project'] == 'drupal') {
        $core[] = array(
          'name' => check_plain($content->info['name']),
          'machinename' => $machinename,
          'type' => $content->info['package'],
          'status' => $content->status,
        );
      }
    }
  }
  $header = array(
    t('Module Name'),
    t('Machine Name'),
    t('Module Type'),
    t('Enable/Disable'),
  );
  // Render table.
  $render_array['maintable'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $core,
  );
  return $render_array;

}

/**
 * This function used to print the other module in the table format.
 */
function module_checker_other_module() {
  $results = system_rebuild_module_data();
  $other = array();
  if (is_array($results) || is_object($results)) {
    foreach ($results as $machinename => $content) {
      if ($content->info['package'] == 'Other') {
        $other[] = array(
          'name' => check_plain($content->info['name']),
          'machinename' => $machinename,
          'type' => $content->info['package'],
          'status' => $content->status,
        );
      }
    }
  }
  $header = array(
    t('Module Name'),
    t('Machine Name'),
    t('Module Type'),
    t('Enable/Disable'),
  );
  // Render table.
  $render_array['maintable'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $other,
  );
  return $render_array;
}
