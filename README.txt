CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
-----------
This module is used to differentiate the custom, contribute and core module.
This module will be used for statistical and dev purposes only.

Uses:

1. Its display all modules by category wise.

2. Easily identify the custom, contribute and core module.
   So it's really helpful for site auditing.

Note: This module is for statistical and development purpose only.
Enable it when ever its needed in the Production Environment.

REQUIREMENTS
------------
This project does not require any support modules

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Customize the menu settings in Administration ? Configuration and modules ?
   Administration ? Administration menu.

 * To prevent administrative menu items from appearing twice, you may hide the
   "Management" menu block.

TROUBLESHOOTING
---------------
 * If the menu does not display, check the following:
  - Check you have administrator permission to view this module.
  - Clear the drupal cache if the menu is not displayed

MAINTAINERS
-----------
Current maintainers:
Jabastin https://www.drupal.org/u/jabastinameex
